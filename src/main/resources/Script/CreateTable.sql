CREATE TABLE if not exists product(
    product_id serial not null PRIMARY KEY,
    product_name varchar(120) not null ,
    product_price float not null
);

CREATE TABLE if not exists customer(
    customer_id serial not null PRIMARY KEY,
    customer_name varchar(120) not null ,
    customer_address varchar(120) not null ,
    customer_phone varchar(11)
);

CREATE TABLE if not exists invoice(
    invoice_id serial not null PRIMARY KEY,
    invoice_date date default now(),
    customer_id int not null ,
    FOREIGN KEY (customer_id) REFERENCES customer (customer_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE if not exists invoice_detail(
    id serial not null PRIMARY KEY,
    invoice_id int not null ,
    product_id int not null ,
    FOREIGN KEY (invoice_id) REFERENCES invoice (invoice_id) ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES product (product_id) ON DELETE CASCADE ON UPDATE CASCADE
);




