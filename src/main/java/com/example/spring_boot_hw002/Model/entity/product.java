package com.example.spring_boot_hw002.Model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class product{
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int productId;
    private String productName;
    private float productPrice;
}
