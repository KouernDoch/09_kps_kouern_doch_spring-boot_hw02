package com.example.spring_boot_hw002.Model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private int invoiceId;
    private Date invoiceDate;
    private Customer customer;
    private List<product> product;
}
