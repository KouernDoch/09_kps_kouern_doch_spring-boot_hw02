package com.example.spring_boot_hw002.Model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiRespnse<T>{
   private LocalDate time;
   private HttpStatus status;
   private String message;
   @JsonInclude(JsonInclude.Include.NON_NULL)
   private T payload;
}
