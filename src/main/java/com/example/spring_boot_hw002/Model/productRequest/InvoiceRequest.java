package com.example.spring_boot_hw002.Model.productRequest;

import com.example.spring_boot_hw002.Model.entity.Customer;
import com.example.spring_boot_hw002.Model.entity.product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequest {
    private Date invoiceDate;
    private int customerId;
    private List<Integer> productId;
}
