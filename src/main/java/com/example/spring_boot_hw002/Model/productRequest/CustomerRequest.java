package com.example.spring_boot_hw002.Model.productRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequest {
    private  String customerName;
    private  String customerPhone;
    private String customerAddress;
}
