package com.example.spring_boot_hw002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHw002Application {

    public static void main(String[] args) {

        SpringApplication.run(SpringBootHw002Application.class, args);
    }

}
