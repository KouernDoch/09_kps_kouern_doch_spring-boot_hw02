package com.example.spring_boot_hw002.Repository;

import com.example.spring_boot_hw002.Model.entity.product;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.DeleteMapping;


import java.util.List;

@Mapper
public interface ProductRepository {
//    @Results(id = "productMap",value = {
//            @Result(property = "productId",column = "product_id"),
//            @Result(property = "productName",column = "product_name"),
//            @Result(property = "productPrice",column = "product_price")
//    })
// Get all product
    @Select("""
            SELECT * FROM product
            """)
//    @ResultMap("productMap")
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
   List<product> getallProduct();

    //Get product by ID
    @Select("""
            SELECT *
             FROM product WHERE product_id=#{id}
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    product getProductByID(int id);

    //Insert Data
    @Select("""
            INSERT INTO product ( product_name , product_price )
            VALUES ( #{pro.productName} , #{pro.productPrice} )
            RETURNING *
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    product insertProduct(@Param("pro") product pro);

    @Update("""
            UPDATE product SET 
            product_name=#{pro.productName},product_price=#{pro.productPrice}
            WHERE  product_id=#{id}
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    void updateProductById(int id, @Param("pro") product productRequest);

    @Delete("""
            DELETE FROM product WHERE product_id=#{id}
            """)
    void deleteProductById(int id);

    //Get all product vai invoice
    @Select("""
            SELECT p.product_id,p.product_name,p.product_price
            FROM product p INNER JOIN invoice_detail iv ON p.product_id = iv.product_id
            WHERE invoice_id=#{id};
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    List<product> getallProductByID( int id);
}
