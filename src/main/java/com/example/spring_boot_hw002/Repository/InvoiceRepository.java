package com.example.spring_boot_hw002.Repository;

import com.example.spring_boot_hw002.Model.entity.Invoice;
import com.example.spring_boot_hw002.Model.productRequest.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {

    // Select all invoice
    @Select("""
            SELECT * FROM invoice
            """)
    @Results(id = "invoiceMapper",value = {
    @Result(property = "invoiceId",column = "invoice_id"),
    @Result(property = "invoiceDate",column = "invoice_date"),
            @Result(property = "customer", column = "customer_id",
                    one = @One(select = "com.example.spring_boot_hw002.Repository.CustomerRepository.getCustomerById")),

            @Result(property = "product", column = "invoice_id",
                    many = @Many(select = "com.example.spring_boot_hw002.Repository.ProductRepository.getallProductByID")
            )
    }
    )
    List<Invoice> getallInvoice();


    @Select("""
            SELECT * FROM invoice WHERE invoice_id=#{id}
            """)
    @ResultMap("invoiceMapper")
    Invoice getInvoiceById(int id);


    @Delete("""
            DELETE FROM invoice WHERE invoice_id=#{id}
            """)
    void deleteInvoiceById(int id);



    void updateInvoiceById(int id, InvoiceRequest invoiceRequest);

    @Select("""
            INSERT INTO invoice (customer_id) 
            VALUES(#{invoice.customerId})
            RETURNING invoice_id
            """)
    Integer insertInvoice(@Param("invoice") InvoiceRequest invoiceRequest);
    @Insert("""
            INSERT INTO invoice_detail(invoice_id,product_id)
            VALUES(#{invoiceId},#{productId})
            """)
    void insertIntoInvoiceDetail(Integer invoiceId,Integer productId);
}
