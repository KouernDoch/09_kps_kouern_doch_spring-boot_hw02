package com.example.spring_boot_hw002.Repository;

import com.example.spring_boot_hw002.Model.entity.Customer;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Select("""
            SELECT * FROM customer 
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    public List<Customer> getallCustomer();

    @Select("""
            SELECT * FROM customer WHERE customer_id=#{id}
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer getCustomerById(int id);

    @Select("""
            INSERT INTO customer (customer_name,customer_phone,customer_address)
            VALUES (#{customer.customerName},#{customer.customerPhone},#{customer.customerAddress})
            RETURNING *
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer insertCustomer(@Param("customer") Customer customer);


    @Update("""
            UPDATE customer SET 
            customer_name=#{customer.customerName},
            customer_phone=#{customer.customerPhone}
            ,customer_address=#{customer.customerAddress}
             WHERE customer_id=#{id}
            """)
    void updateCustomerById(int id,@Param("customer") Customer customerRequest);


    @Delete("""
            DELETE FROM customer WHERE customer_id=#{id}
            """)
    void deleteCustomerById(int id);
}
