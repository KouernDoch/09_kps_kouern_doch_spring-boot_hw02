package com.example.spring_boot_hw002.Controller;
import com.example.spring_boot_hw002.Model.response.ApiRespnse;
import com.example.spring_boot_hw002.Service.Productservice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.spring_boot_hw002.Model.entity.product;


import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProductController {
private final Productservice productservice;
    public ProductController(Productservice productservice) {
        this.productservice = productservice;
    }

    //Get all product
    @GetMapping("/get_all_products")
    public ResponseEntity<?> getallProduct(){
        ApiRespnse<List<product>> respnse=new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                productservice.getallProduct()
        );
        return ResponseEntity.ok(respnse);
    }
    // Get product by ID
    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<?> getProductByID(@PathVariable int id){
        ApiRespnse<product> respnse=new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                productservice.getProductByID(id)
        );
        return ResponseEntity.ok(respnse);
    }

    //Add product
    @PostMapping("/add-new-product")
    public ResponseEntity<?> insertProduct(@RequestBody product pro){
      return ResponseEntity.ok(new ApiRespnse<product>(
              LocalDate.now(),
              HttpStatus.OK,
              "Successfully",
              productservice.insertProduct(pro)
      ));
    }
    //Update product

    @PutMapping("/update-product-by-id/{id}")
    public ResponseEntity<?> updateProductById(@PathVariable int id, @RequestBody product productRequest){
        productservice.updateProductById(id,productRequest);
        return ResponseEntity.ok(new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
               null
        ));
    }
    // Delete product
    @DeleteMapping("/delete-product-by-id/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable int id){
        productservice.deleteById(id);
        return ResponseEntity.ok(new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));
    }

}
