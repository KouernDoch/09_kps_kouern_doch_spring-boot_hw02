package com.example.spring_boot_hw002.Controller;

import com.example.spring_boot_hw002.Model.entity.Customer;
import com.example.spring_boot_hw002.Model.entity.product;
import com.example.spring_boot_hw002.Model.response.ApiRespnse;
import com.example.spring_boot_hw002.Service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

//Get all customer
    @GetMapping("/get_all_Customer")
public ResponseEntity<?> getallCustomer(){
    ApiRespnse<List<Customer>> respnse=new ApiRespnse<>(
            LocalDate.now(),
            HttpStatus.OK,
            "Successfully",
            customerService.getallCustomer()
    );
    return ResponseEntity.ok(respnse);
}
// Get Customer by ID

    @GetMapping("/get-Customer-by-id/{id}")
    public ResponseEntity<?> getCustomerByID(@PathVariable int id){
        ApiRespnse<Customer> respnse=new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                customerService.getCustomerByID(id)
        );
        return ResponseEntity.ok(respnse);
    }

    // Insert Customer
    @PostMapping("/add-new-Customer")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer customer){
        return ResponseEntity.ok(new ApiRespnse<Customer>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                customerService.insertCustomer(customer)
        ));
    }

    //Update Customer
    @PutMapping("/update-Customer-by-id/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable int id, @RequestBody Customer customerRequest){
        customerService.updateCustomerById(id,customerRequest);
        return ResponseEntity.ok(new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));
    }

    //Delete Customer
    @DeleteMapping("/delete-Customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable int id){
        customerService.deleteCustomerById(id);
        return ResponseEntity.ok(new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));
    }

}
