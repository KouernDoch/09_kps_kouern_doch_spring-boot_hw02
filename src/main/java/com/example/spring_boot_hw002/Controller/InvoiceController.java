package com.example.spring_boot_hw002.Controller;

import com.example.spring_boot_hw002.Model.entity.Customer;
import com.example.spring_boot_hw002.Model.entity.Invoice;
import com.example.spring_boot_hw002.Model.entity.product;
import com.example.spring_boot_hw002.Model.productRequest.InvoiceRequest;
import com.example.spring_boot_hw002.Model.response.ApiRespnse;
import com.example.spring_boot_hw002.Service.InvoiceService;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    public ResponseEntity<?> getallInvoice(){
      return ResponseEntity.ok(new ApiRespnse<List<Invoice>>(
              LocalDate.now(),
              HttpStatus.OK,
              "Successfully",
              invoiceService.getallInvoice()
      ));
    }

    @GetMapping("/get-Invoice-by-id/{id}")
    public ResponseEntity<?> getInvoiceByID(@PathVariable int id){
        ApiRespnse<Invoice> respnse=new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                invoiceService.getInvoiceByID(id)
        );
        return ResponseEntity.ok(respnse);
    }

    @PostMapping("/add-new-invoice")
    public ResponseEntity<?> insertInvoice(@RequestBody InvoiceRequest invoiceRequest){
        return ResponseEntity.ok(new ApiRespnse<List<Invoice>>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));
    }

    @DeleteMapping("/delete-Invoice-by-id/{id}")
    public ResponseEntity<?> deleteInvoice(@PathVariable int id){
        invoiceService.deleteInvoiceById(id);
        return ResponseEntity.ok(new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));
    }

    @PutMapping("/update-Invoice-by-id/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable int id, @RequestBody InvoiceRequest invoiceRequest){
        invoiceService.updateInvoiceById(id,invoiceRequest);
        return ResponseEntity.ok(new ApiRespnse<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));
    }

}
