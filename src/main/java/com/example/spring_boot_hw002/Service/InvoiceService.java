package com.example.spring_boot_hw002.Service;

import com.example.spring_boot_hw002.Model.entity.Invoice;
import com.example.spring_boot_hw002.Model.productRequest.InvoiceRequest;

import java.util.List;

public interface InvoiceService {

    List<Invoice> getallInvoice();

    Invoice getInvoiceByID(int id);

    void insertInvoice(InvoiceRequest invoiceRequest);


    void deleteInvoiceById(int id);

    void updateInvoiceById(int id, InvoiceRequest invoiceRequest);
}
