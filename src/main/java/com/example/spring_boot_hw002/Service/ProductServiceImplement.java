package com.example.spring_boot_hw002.Service;
import com.example.spring_boot_hw002.Model.entity.product;
import com.example.spring_boot_hw002.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProductServiceImplement implements Productservice {
    private ProductRepository productRepository;
    @Autowired
    public void ProductServiceImplement(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    @Override
    public List<product> getallProduct() {
        return productRepository.getallProduct();
    }

    @Override
    public product getProductByID(int id) {
        return productRepository.getProductByID(id);
    }

    @Override
    public product insertProduct(product pro) {
        return productRepository.insertProduct(pro);
    }

    @Override
    public void updateProductById(int id, product productRequest) {
        productRepository.updateProductById(id,productRequest);
    }

    @Override
    public void deleteById(int id) {
        productRepository.deleteProductById(id);
    }
}
