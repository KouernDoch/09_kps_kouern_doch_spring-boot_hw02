package com.example.spring_boot_hw002.Service;

import com.example.spring_boot_hw002.Model.entity.Customer;
import com.example.spring_boot_hw002.Repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImplement implements CustomerService{

    private CustomerRepository customerRepository;
    @Autowired
    public void CustomerServiceImplement(CustomerRepository customerRepository){
        this.customerRepository=customerRepository;
    }

    @Override
    public Customer getCustomerByID(int id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public List<Customer> getallCustomer() {
        return customerRepository.getallCustomer();
    }

    @Override
    public Customer insertCustomer(Customer customer) {
        return customerRepository.insertCustomer(customer);
    }

    @Override
    public void updateCustomerById(int id, Customer customerRequest) {
        customerRepository.updateCustomerById(id,customerRequest);
    }

    @Override
    public void deleteCustomerById(int id) {
        customerRepository.deleteCustomerById(id);
    }
}
