package com.example.spring_boot_hw002.Service;

import com.example.spring_boot_hw002.Model.entity.Customer;

import java.util.List;

public interface CustomerService {

    public Customer getCustomerByID(int id);
    public List<Customer> getallCustomer();

    Customer insertCustomer(Customer customer);

    void updateCustomerById(int id, Customer customerRequest);

    void deleteCustomerById(int id);
}
