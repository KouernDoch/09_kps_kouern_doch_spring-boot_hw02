package com.example.spring_boot_hw002.Service;
import com.example.spring_boot_hw002.Model.entity.product;

import java.util.List;


public interface Productservice {
  List<product> getallProduct();

    product getProductByID(int id);

    product insertProduct(product pro);

  void updateProductById(int id, product pro);

  void deleteById(int id);

}
