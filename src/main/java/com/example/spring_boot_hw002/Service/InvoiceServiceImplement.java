package com.example.spring_boot_hw002.Service;

import com.example.spring_boot_hw002.Model.entity.Invoice;
import com.example.spring_boot_hw002.Model.productRequest.InvoiceRequest;
import com.example.spring_boot_hw002.Repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImplement implements InvoiceService {

    private InvoiceRepository invoiceRepository;

    @Autowired
    private void InvoiceServiceImplement(InvoiceRepository invoiceRepository){
        this.invoiceRepository=invoiceRepository;
    }
    @Override
    public List<Invoice> getallInvoice() {
        return invoiceRepository.getallInvoice();
    }

    @Override
    public Invoice getInvoiceByID(int id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public void insertInvoice(InvoiceRequest invoiceRequest) {
      Integer invoiceid = invoiceRepository.insertInvoice(invoiceRequest);
        for (int productId:invoiceRequest.getProductId()) {
            invoiceRepository.insertIntoInvoiceDetail(invoiceid,productId);
        }
    }

    @Override
    public void deleteInvoiceById(int id) {
        invoiceRepository.deleteInvoiceById(id);
    }

    @Override
    public void updateInvoiceById(int id, InvoiceRequest invoiceRequest) {
        invoiceRepository.updateInvoiceById(id,invoiceRequest);
    }


}
